GICS Configuration Management
============================

This repository contains
[Ansible](http://docs.ansible.com/ansible/index.html) "playbooks" for
managing the configuration of the ICEX GPS Interface Computer.

## Prerequisites

The control computer used to manage the configuration must be running
Linux or Unix (e.g. OS X). Windows cannot be used for control. Visit the
link above for Ansible installation instructions.

## GICS Preparation

The initial Debian OS installation must be performede. This will configure
the system to use DHCP to obtain its IP address. Login as `root` via the
console and run the command `ifconfig eth0` to read the IP address.

### SSH Public Key

To avoid being prompted for a password, you should configure the `root`
and `icex` accounts to allow you to log-in with a public key. The easiest
way to do this is by running the following command from the control
computer:

```
ssh-copy-id root@IPADDR
ssh-copy-id icex@IPADDR
```

Replace *IPADDR* with the IP address of GICS. Note that this step assumes
that you have already created an SSH key-pair on the control computer.

## Inventory File

Create a file named `systems` in the repository directory and include the
following content:

```
[gics]
HOSTNAME ansible_ssh_host=IPADDR
```

Where *HOSTNAME* is the hostname for the node (e.g. gics-1) and *IPADDR* is
the DHCP IP address.

## Configuration

Run the following command from the top-level directory of the repository:

```shell
ansible-playbook -i systems provision.yaml
```

Consult the online Ansible documentation for further information.
